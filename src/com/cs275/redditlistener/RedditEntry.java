package com.cs275.redditlistener;

/**
 * Object that retains a Reddit post
 * 
 * @author Alex Nedelcu
 *
 */
public class RedditEntry {

	public String _title;
	public String _imgProfile;
	public String _category;
	public String _permalink;
	public Integer _score;
	
	/**
	 * Constructs and initializes a RedditEntry 
	 * 
	 * @param pTitle
	 * @param pImgProfile
	 * @param pCategory
	 * @param pScore
	 * @param pURL
	 */
	RedditEntry(String pTitle, String pImgProfile, String pCategory, Integer pScore, String pURL) {
		_title = pTitle;
		_imgProfile = pImgProfile;
		_category = pCategory;
		_score = pScore;
		_permalink = pURL;
	}

}
