package com.cs275.redditlistener;

import java.io.InputStream;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RedditCustomAdapter extends ArrayAdapter<RedditEntry> {

	Context context;
	int layoutResourceId;
	ArrayList<RedditEntry> data = new ArrayList<RedditEntry>();

	/**
	 * Constructs the adapter
	 * 
	 * @param context
	 * @param layoutResourceId
	 * @param data
	 */
	public RedditCustomAdapter(Context context, int layoutResourceId, ArrayList<RedditEntry> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}


	/**
	 * sets the post info into the list adapter
	 */
	@SuppressLint({ "NewApi", "SimpleDateFormat" })
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		EntryHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			holder = new EntryHolder();
			holder.txtTitle = (TextView) row.findViewById(R.id.txtTitlex);
			holder.txtAuthor = (TextView) row.findViewById(R.id.txtAuthor);
			holder.txtScore = (TextView) row.findViewById(R.id.txtScore);
			holder.imgProfile = (ImageView) row.findViewById(R.id.imgProfile);
			row.setTag(holder);
		} else {
			holder = (EntryHolder) row.getTag();
		}
		RedditEntry w = data.get(position);

		// download and show the icon
		new DownloadImageTask(holder.imgProfile).execute(w._imgProfile);

		holder.txtScore.setText(String.valueOf(w._score));
		holder.txtTitle.setText(w._title);
		holder.txtAuthor.setText("By " + w._category);
		
		return row;

	}
	
	/**
	 * Downloads the image, gets the bitmap data and assigns it
	 * to a given image
	 * 
	 * @author Alex Nedelcu
	 *
	 */
	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	    ImageView bmImage;

	    /**
	     * Constructor
	     * 
	     * @param bmImage	the ImageView of the graphic element
	     */
	    public DownloadImageTask(ImageView bmImage) {
	        this.bmImage = bmImage;
	    }

	    /**
	     * Downloads the image
	     * 
	     * @param urls list of URLs of images
	     */
	    protected Bitmap doInBackground(String... urls) {
	        String urldisplay = urls[0];
	        Bitmap mIcon11 = null;
	        try {
	            InputStream in = new java.net.URL(urldisplay).openStream();
	            mIcon11 = BitmapFactory.decodeStream(in);
	        } catch (Exception e) {
	            Log.e("Error", e.getMessage());
	            e.printStackTrace();
	        }
	        return mIcon11;
	    }

	    /**
	     * Assigns the image bitmap to the image view
	     * 
	     * @param result
	     */
	    protected void onPostExecute(Bitmap result) {
	        bmImage.setImageBitmap(result);
	    }
	}

	static class EntryHolder {
		TextView txtTitle;
		TextView txtAuthor;
		TextView txtScore;
		ImageView imgProfile;
	}

}
