package com.cs275.redditlistener;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * Reddit class will retrieve the Reddit posts
 * and build data objects based on it
 * 
 * @author		Alexandru Nedelcu
 */
public class Reddit {
	private static String _url = "http://reddit.com/.json";

	static ArrayList<RedditEntry> _entriesArray = new ArrayList<RedditEntry>();
	
	Reddit(String pURL) {
		_url = pURL;
	}
	
	/**
	* Loads the data from Reddit
	* 
	* @param	pURL	the URL of the JSON view: e.g. http://reddit.com/.json
	*/
	public void loadData() throws IOException {
		
		// Connect to the URL
		URL url = new URL(_url);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();

		// get the root element
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		
		 JsonArray entries = root.getAsJsonObject().get("data").getAsJsonObject().get("children").getAsJsonArray();
		
		 String title;
		 String imgProfile;
		 String category;
		 String permalink;
		 Integer score;
		for (int i=0; i<entries.size(); i++) {
			title = entries.get(i).getAsJsonObject().get("data").getAsJsonObject().get("title").getAsString();
			imgProfile = entries.get(i).getAsJsonObject().get("data").getAsJsonObject().get("thumbnail").getAsString();
			category = entries.get(i).getAsJsonObject().get("data").getAsJsonObject().get("author").getAsString();
			score = entries.get(i).getAsJsonObject().get("data").getAsJsonObject().get("score").getAsInt();
			permalink = entries.get(i).getAsJsonObject().get("data").getAsJsonObject().get("permalink").getAsString();
			
			_entriesArray.add(new RedditEntry(title, imgProfile, category, score, permalink));
		}
	}
	

	
	
	/**
	* Get the Reddit entry list
	* 
	* @return		the entry list
	*/
	public ArrayList<RedditEntry> getRedditEntryList(int score) {

		ArrayList<RedditEntry> _filtered = new ArrayList<RedditEntry>();
		for (int i=0; i<_entriesArray.size(); i++) {
			if (_entriesArray.get(i)._score >= score)
				_filtered.add(_entriesArray.get(i));
		}
		return _filtered;
	}
	
	
}
 