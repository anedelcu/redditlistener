package com.cs275.redditlistener;

import java.io.IOException;
import java.util.ArrayList;

import android.os.Bundle;
import android.os.Handler;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.text.InputType;
/**
 * 
 * @author Alex Nedelcu
 *
 */
public class MainActivity extends Activity {

	final Handler mHandler = new Handler();
	private Reddit initialRedditPostList;
	private ArrayList<RedditEntry> _displayedRedditPostList;
	private int _score = 0;
	public final static String EXTRA_MESSAGE = "com.example.MainActivity.OpenURL";

	
	/**
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// get the Reddit posts and display them
		retrieve();

		// set action on click on a list item
		ListView redditList = (ListView) findViewById(R.id.lstPosts);
		redditList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				// create intent and display the permalink into a webview
				Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
				intent.putExtra(EXTRA_MESSAGE, "http://www.reddit.com"
						+ _displayedRedditPostList.get(arg2)._permalink);
				startActivity(intent);

			}

		});

	}

	
	/**
	 * Creates a Alert Dialog that lets the user set a new score
	 * It creates a score text box which allows only numbers to 
	 * be inserted.
	 */
	@SuppressLint("NewApi")
	public void showDialog(View v) {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				MainActivity.this);

		// set title
		alertDialogBuilder.setTitle("Enter new score");

		// create the EditBox
		final EditText lEditText = new EditText(this);
		if (_score > 0)
			lEditText.setText(String.valueOf(_score));
		lEditText.setInputType(InputType.TYPE_CLASS_NUMBER);

		// build the dialog, set the buttons and their actions
		alertDialogBuilder
			.setCancelable(false)
			.setPositiveButton("Set",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						/*
						 * on click on the "Set" button, close the dialog,
						 * update the score value and render the post list again
						 */
						dialog.cancel();
						_score = Integer.parseInt(lEditText.getText()
								.toString());
						renderReddit();
					}
				})
			.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
					}).setView(lEditText);

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	
	/**
	 * Renders the loaded JSON code into the UI
	 */
	public void renderReddit() {

		ListView lst = (ListView) findViewById(R.id.lstPosts);
		
		/*
		 * Populates the list in the UI using the custom adapter
		 */
		_displayedRedditPostList = initialRedditPostList.getRedditEntryList(_score);
		RedditCustomAdapter userAdapter = new RedditCustomAdapter(
				MainActivity.this, R.layout.custom_list_entry,
				_displayedRedditPostList);
		lst.setItemsCanFocus(false);
		lst.setAdapter(userAdapter);
		
		/* 
		 * Shows the list and hides the loading icon
		 */
		lst.setVisibility(View.VISIBLE);
		findViewById(R.id.prgLoadingReddit).setVisibility(View.GONE);
	}

	/**
	 * Sets up a HTTP connection which gets the JSON representation of
	 * the Reddit's posts from the front page. They are not filtered by
	 * score. This step will happen later in the process
	 */
	public void retrieve() {

		ListView lst = (ListView) findViewById(R.id.lstPosts);
		
		/*
		 * Hides the list until the JSON loading finishes. 
		 * A loading icon is shown till then.
		 */
		lst.setVisibility(View.GONE);

		/*
		 * Loads the JSON by creating a separate thread from the UI
		 */
		Thread t = new Thread() {
			public void run() {

				initialRedditPostList = new Reddit("http://www.reddit.com/.json");
				try {
					initialRedditPostList.loadData();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				/*
				 * Updates the UI, by calling renderReddit(), 
				 * in the UI thread
				 */
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						renderReddit();
					}
				});
			}
		};
		t.start();
	}

}
